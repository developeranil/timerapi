<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class UserScope implements Scope
{
    /**
     * --------------------------------------------------
     * attach the organization scope for non super organizations.
     * --------------------------------------------------
     * @param Builder $builder
     * @param Model $model
     * --------------------------------------------------
     */
    public function apply(Builder $builder, Model $model)
    {
        if (!session()->get('is_super')) {
            $builder->where($model->getTable() . '.user_id', session()->get('user_id'));
        }
    }
}
