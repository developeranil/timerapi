<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoginLog extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     * @var array $fillable
     */
    protected $fillable = ['user_id', 'ip', 'agent', 'login_date', 'login_time'];

    /**
     * The attributes that should be hidden for arrays.
     * @var array $hidden
     */
    protected $hidden = ['updated_at'];

    /**
     * --------------------------------------------------
     * get the user from the log.
     * --------------------------------------------------
     * @return BelongsTo
     * --------------------------------------------------
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
