<?php

namespace App\Models;

use App\Scopes\UserScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array $fillable
     */
    protected $fillable = ['user_id', 'salary_type', 'salary_amount', 'bonus', 'overtime', 'cash_advance', 'start_date_time', 'end_date_time', 'tax', 'designation', 'salary_structure', 'total_amount', 'description', 'remark'];

    /**
     * The attributes that should be hidden for arrays.
     * @var array $hidden
     */
    protected $hidden = ['updated_at'];

    /**
     * --------------------------------------------------
     * attach the organization scope.
     * --------------------------------------------------
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new UserScope());
    }

    /**
     * --------------------------------------------------
     * get the organization belongs to the user.
     * --------------------------------------------------
     * @return BelongsTo
     * --------------------------------------------------
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
