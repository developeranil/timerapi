<?php

namespace App\Http\Resources;

use App\Libraries\Identity;
use App\Libraries\Space;
use DateTime;
use Exception;
use Illuminate\Http\Resources\Json\JsonResource;

class BaseResource extends JsonResource
{
    protected $space;
    protected $identity;

    public function __construct($resource, $wrap = false)
    {
        parent::__construct($resource);
        // data wrapper
        static::$wrap = $wrap ? 'data' : null;
        // init space
        $this->space = new Space;
        // init identity
        $this->identity = new Identity;
    }

    /**
     * --------------------------------------------------
     * cast the given time to 12HR format.
     * --------------------------------------------------
     * @param string|null $time
     * @return string|null
     * --------------------------------------------------
     */
    protected function formatTime($time): ?string
    {
        try {
            $dateTime = new DateTime($time);
            return $dateTime->format('h:i A');
        } catch (Exception $exception) {
            return null;
        }
    }
}
