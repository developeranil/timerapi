<?php

namespace App\Libraries;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

final class Common
{
    /**
     * --------------------------------------------------
     * convert start and end date of current nepali month
     * to english date.
     * --------------------------------------------------
     * @param bool $formatted
     * @return array
     * --------------------------------------------------
     */
    public static function currentMonthRange($formatted = false): array
    {
        $todayEn = Carbon::now()->toDateString();
        $nepaliYearMonth = preg_replace('/-\d{2}$/m', '', Calendar::select(['date_np'])->convertDate($todayEn)->first()->date_np);
        $calendar = Calendar::select([DB::raw('MIN(`date_en`) AS `start_date`'), DB::raw('MAX(`date_en`) AS `end_date`')])
            ->where(DB::raw('DATE_FORMAT(`date_np`, \'%Y-%m\')'), $nepaliYearMonth)->first();
        if ($formatted) {
            return [$calendar->start_date, $calendar->end_date];
        }
        return $calendar;
    }

    /**
     * --------------------------------------------------
     * convert start and end date of current nepali year
     * to english date.
     * --------------------------------------------------
     * @param bool $formatted
     * @return array
     * --------------------------------------------------
     */
    public static function currentYearRange($formatted = false): array
    {
        $todayEn = Carbon::now()->toDateString();
        $nepaliYear = preg_replace('/\-\d{2}\-\d{2}$/im', '', Calendar::select(['date_np'])->convertDate($todayEn)->first()->date_np);
        $calendar = Calendar::select([DB::raw('MIN(`date_en`) AS `start_date`'), DB::raw('MAX(`date_en`) AS `end_date`')])
            ->where(DB::raw('DATE_FORMAT(`date_np`, \'%Y\')'), $nepaliYear)->first();
        if ($formatted) {
            return [$calendar->start_date, $calendar->end_date];
        }
        return $calendar;
    }

    /**
     * --------------------------------------------------
     * change the date from Y-M-D to D/M/Y format.
     * --------------------------------------------------
     * @param string $date
     * @return string
     * --------------------------------------------------
     */
    public static function formatNepaliDate($date): string
    {
        $components = explode('-', $date);
        if ($date === null || count($components) !== 3) {
            return null;
        }
        [$year, $month, $day] = $components;
        return $year . '-' . $month . '-' . $day;
    }

    /**
     * --------------------------------------------------
     * validate the mobile number format for Nepal.
     * --------------------------------------------------
     * @param string $number
     * @param bool $includeNumber
     * @return bool|string
     * --------------------------------------------------
     */
    public static function validPhone(string $number, bool $includeNumber = false)
    {
        $number = str_replace([' ', '(', ')', '-'], '', trim($number));
        if (preg_match('/^(?:0|(?:^\+|00)?977)?(?<number>9(?:8[0124568]|7[45]|6[12])\d{7})$/', $number, $matches)) {
            if ($includeNumber) {
                return $matches['number'];
            }
            return true;
        }
        return false;
    }

    /**
     * --------------------------------------------------
     *  generate random number (token/otp) in defined length.
     * --------------------------------------------------
     * @param int $length
     * @return string
     * --------------------------------------------------
     */
    public static function randomToken(int $length = 6): string
    {
        $sequence = [0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946, 17711, 28657, 46368, 75025];
        return substr(str_shuffle(implode('', $sequence)), 0, 6);
    }

    /**
     * --------------------------------------------------
     * generate unique hash for message
     * --------------------------------------------------
     * @param string $model
     * @param string $field
     * @param int $length
     * @return string
     * --------------------------------------------------
     */
    public static function identifier($model = User::class, string $field = 'identifier', int $length = 64): string
    {
        $hash = null;
        while (true) {
            $hash = substr(hash('SHA256', microtime(false)), 0, $length);
            $message = $model::where($field, $hash)->first();
            if (!isset($message->id)) {
                break;
            }
        }
        return $hash;
    }
}
