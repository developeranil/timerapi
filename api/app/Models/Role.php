<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array $fillable
     */
    protected $fillable = ['role', 'description','is_active'];

    /**
     * The attributes that should be hidden for arrays.
     * @var array $hidden
     */
    protected $hidden = ['updated_at'];
}
