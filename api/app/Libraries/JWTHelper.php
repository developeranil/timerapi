<?php

namespace App\Libraries;

use Carbon\Carbon;
use Illuminate\Http\Request;

class JWTHelper
{
    private $request;
    protected $secret;
    protected $headers;
    protected $payload;
    protected $signature;

    public function __construct(Request $request)
    {
        // init resources
        $this->secret = env('JWT_SECRET', 'fb9043f7f2c3c13b35849901d870fffe2c4d8c3d8c00a5e186cfab4b9e9b12b8');
        $this->request = $request;

        // inject headers
        $this->setHeaders();
    }

    /**
     * --------------------------------------------------
     * encode the string to base64 and remove special
     * characters.
     * --------------------------------------------------
     * @param string $data
     * @return string
     * --------------------------------------------------
     */
    private function base64UrlEncode(string $data): string
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    /**
     * --------------------------------------------------
     * decode the bas64 string and add special characters.
     * --------------------------------------------------
     * @param string $data
     * @return string
     * --------------------------------------------------
     */
    private function base64UrlDecode(string $data): string
    {
        return base64_decode(strtr($data, '-_', '+/'), true);
    }

    /**
     * --------------------------------------------------
     * encode the string and set JWT headers
     * --------------------------------------------------
     * @return $this
     * --------------------------------------------------
     */
    private function setHeaders(): self
    {
        $this->headers = $this->base64UrlEncode(json_encode([
            'typ' => 'JWT',
            'alg' => 'HS256'
        ]));
        return $this;
    }

    /**
     * --------------------------------------------------
     * encode the sting and set JWT payload
     * --------------------------------------------------
     * @param array $data
     * @return $this
     * --------------------------------------------------
     */
    protected function setPayload($data = []): self
    {
        $this->payload = $this->base64UrlEncode(json_encode($data));
        // inject signature
        $this->setSignature();

        return $this;
    }

    /**
     * --------------------------------------------------
     * encode the string and set JWT signature.
     * --------------------------------------------------
     * @return $this
     * --------------------------------------------------
     */
    private function setSignature(): self
    {
        $this->signature = $this->base64UrlEncode(hash_hmac('sha256', $this->headers . '.' . $this->payload, $this->secret, false));
        return $this;
    }

    /**
     * --------------------------------------------------
     * concat the headers, payload and signature to create
     * the JWT token.
     * --------------------------------------------------
     * @return string
     * --------------------------------------------------
     */
    protected function getToken(): string
    {
        return implode('.', [$this->headers, $this->payload, $this->signature]);
    }

    /**
     * --------------------------------------------------
     * extract the payload from the validated token.
     * --------------------------------------------------
     * @param string $token
     * @return mixed
     * --------------------------------------------------
     */
    protected function getPayload(string $token)
    {
        // extract the payload
        [$requestHeaders, $requestPayload, $requestSignature] = explode('.', $token);
        return json_decode($this->base64UrlDecode($requestPayload), false);
    }

    /**
     * --------------------------------------------------
     * generate the fingerprint based on user ip and agent.
     * --------------------------------------------------
     * @return string
     * --------------------------------------------------
     */
    protected function getFingerprint(): string
    {
        return hash('sha256', env('JWT_SECRET') . '|' . $this->request->userAgent());
    }

    /**
     * --------------------------------------------------
     * validate the current and requested fingerprint.
     * --------------------------------------------------
     * @param string $fingerprint
     * @return bool
     * --------------------------------------------------
     */
    private function validateFingerprint(string $fingerprint): bool
    {
        return $fingerprint === $this->getFingerprint();
    }

    /**
     * --------------------------------------------------
     * extract the signature from JWT token and validate.
     * --------------------------------------------------
     * @param string $token
     * @return bool
     * --------------------------------------------------
     */
    protected function validate(string $token): bool
    {
        // validate the format
        $parts = explode('.', $token);
        if (count($parts) !== 3) {
            return false;
        }

        // init the resources
        list($requestHeaders, $requestPayload, $requestSignature) = $parts;

        // validate signature
        $buildSignature = $this->base64UrlEncode(hash_hmac('sha256', $requestHeaders . '.' . $requestPayload, $this->secret, false));
        if ($buildSignature !== $requestSignature) {
            return false;
        }

        // validate the time
        $payload = json_decode($this->base64UrlDecode($requestPayload), false);
        $timestamp = Carbon::now()->timestamp;
        return ($payload->expire >= $timestamp) && $this->validateFingerprint($payload->fp);
    }
}
