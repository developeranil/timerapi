<?php

namespace App\Http\Requests;

use App\Libraries\Identity;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;
use stdClass;

class BaseFormRequest extends FormRequest
{
    /**
     * @var Identity $identity
     */
    protected $identity;

    public function __construct(Request $request)
    {
        parent::__construct();

        // init the identity provider
        $this->identity = new Identity();

        // per page validation
        if ($request->has('per_page')) {
            if ($request->get('per_page') === 0) {
                $request->merge(['per_page' => false]);
            }
        } else {
            $request->merge(['per_page' => env('PER_PAGE', 15)]);
        }
    }

    public function identity(): Identity
    {
        return $this->identity;
    }

    /**
     * --------------------------------------------------
     * override failed validation and format errors.
     * --------------------------------------------------
     * @param Validator $validator
     * @throws ValidationException
     * --------------------------------------------------
     */
    protected function failedValidation(Validator $validator): void
    {
        // format errors
        $errors = $this->formatErrors($validator->errors());
        throw new ValidationException($validator, response()->json(['errors' => $errors], 422));
    }


    /**
     * --------------------------------------------------
     * format the errors in key/value pair object.
     * --------------------------------------------------
     * @param MessageBag $errors
     * @return stdClass
     * --------------------------------------------------
     */
    private function formatErrors(MessageBag $errors): stdClass
    {
        $formatted = new stdClass();
        foreach ($errors->messages() as $key => $error) {
            if (preg_match('/^(?<full>[a-zA-Z0-9_\-]+\.(?<row>\d+)\.(?<field>[a-zA-Z0-9_\-]+))$/im', $key, $elements)) {
                // multiple arrays in single array
                $matches = $elements;
            } elseif (preg_match('/^(?<full>(?<field>[a-zA-Z0-9_\-]+)\.(?<row>\d+))$/im', $key, $values)) {
                // multi values in single array
                $matches = $values;
            }

            if (!empty($matches)) {
                $row = $matches['row'] + 1;
                $replace_key = str_replace($matches['full'], $matches['field'] . '_' . $row, $key);
                $replace_text = str_singular($matches['field']) . ' value at row ' . $row;
                $formatted->$replace_key = preg_replace('/' . $matches['full'] . '(\s+field)?/im', $replace_text, $error[0]);
            } else {
                $formatted->$key = $error[0];
            }
        }
        return $formatted;
    }
}
