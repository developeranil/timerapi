<?php

namespace App\Models;

use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes, Searchable;
    /**
     * The attributes that are mass assignable.
     * @var array $fillable
     */
    protected $fillable = [ 'role_id', 'provider', 'organization_name', 'first_name', 'last_name', 'middle_name', 'position', 'marital_status', 'gender', 'dob', 'joining_date', 'specialization', 'experience', 'educational', 'token', 'password', 'fingerprint', 'photo_url', 'address', 'description', 'remark', 'last_online', 'is_active', 'is_verified'];

    /**
     * The attributes that should be hidden for arrays.
     * @var array $hidden
     */
    protected $hidden = ['password', 'updated_at'];

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'last_online' => 'timestamp'
    ];
}
