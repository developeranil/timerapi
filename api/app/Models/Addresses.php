<?php

namespace App\Models;

use App\Scopes\UserScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Addresses extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'address_name', 'address_type', 'country', 'state', 'city', 'street', 'address', 'pin_code', 'is_active'];

    protected $hidden = ['updated_at'];

    /**
     * --------------------------------------------------
     * attach the organization scope.
     * --------------------------------------------------
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new UserScope());
    }


    /**
     * --------------------------------------------------
     * get the user details from the contact.
     * --------------------------------------------------
     * @return BelongsTo
     * --------------------------------------------------
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
