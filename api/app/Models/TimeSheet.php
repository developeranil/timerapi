<?php

namespace App\Models;

use App\Scopes\UserScope;
use App\Traits\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class TimeSheet extends Model
{
    use Searchable, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     * @var array $fillable
     */
    protected $fillable = ['user_id', 'login_date', 'time_from', 'time_to',  'overtime', 'overtime_rate', 'hourly_rate', 'status', 'duration','description', 'remark',];

    /**
     * The attributes that should be hidden for arrays.
     * @var array $hidden
     */
    protected $hidden = ['updated_at'];


    /**
     * --------------------------------------------------
     * attach the user scope.
     * --------------------------------------------------
     */
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new UserScope());
    }

    /**
     * --------------------------------------------------
     * get the user details from the contact.
     * --------------------------------------------------
     * @return BelongsTo
     * --------------------------------------------------
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
