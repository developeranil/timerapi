<?php

namespace App\Libraries;

use App\Models\Token;
use Carbon\Carbon;
use Illuminate\Http\Request;

final class TokenAuth
{
    /**
     * @var Request $request
     */
    private $request;

    /**
     * @var string $token
     */
    private $authToken;

    public function __construct(Request $request)
    {
        // init resources
        $this->request = $request;
        $this->authToken = hash('SHA256', $request->header('X-Auth-Token'));
    }

    /**
     * --------------------------------------------------
     * validate the auth token and update session.
     * --------------------------------------------------
     * @return bool
     * --------------------------------------------------
     */
    public function validate(): bool
    {
        $token = Token::whereToken($this->authToken)->first();
        if (isset($token->id)) {
            // update token
            $token->last_used_ip = $this->request->ip();
            $token->last_used_at = Carbon::now()->toDateTimeString();
            ++$token->hits;
            $token->save();
            // update session
            session()->put('user_id', $token->user_id);

            return true;
        }
        return false;
    }
}
