<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id'); //reference user
            $table->string('address_name');
            $table->enum('address_type', ['PERMANENT', 'BILLING', 'TEMPORARY'])->default('PERMANENT');
            $table->string('country');
            $table->string('state');
            $table->string('city');
            $table->string('street');
            $table->string('address')->nullable();
            $table->string('pin_code')->nullable();
            $table->unsignedTinyInteger('is_active')->default(1);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
