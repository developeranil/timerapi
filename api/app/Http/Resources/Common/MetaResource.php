<?php

namespace App\Http\Resources\Common;

use App\Http\Resources\BaseResource;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MetaResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'provider' => 'SajiloBachat Pvt. Ltd.',
            'homepage' => 'https://sajilobachat.com',
            'version' => env('API_VERSION', '0.1'),
            'timestamp' => Carbon::now()->timestamp
        ];
    }
}
