<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id'); // app user ID
            $table->enum('salary_type', ['MONTHLY', 'WEEKLY', 'HOURLY']);
            $table->decimal('salary_amount', 8, 2)->default(0);
            $table->decimal('bonus', 8, 2)->default(0);
            $table->decimal('overtime', 9, 2)->default(0);
            $table->decimal('cash_advance', 9, 2)->default(0);
            $table->dateTime('start_date_time')->nullable();
            $table->dateTime('end_date_time')->nullable();
            $table->decimal('tax', 9, 2)->default(0);
            $table->string('designation');
            $table->string('salary_structure');
            $table->text('description')->nullable();
            $table->text('remark')->nullable();
            $table->decimal('total_amount', 9, 2)->default(0);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
