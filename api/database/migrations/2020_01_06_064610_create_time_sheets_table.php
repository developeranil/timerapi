<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTimeSheetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('time_sheets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id'); //reference user
            $table->date('login_date');
            $table->time('time_from');
            $table->time('time_to');
            $table->text('description')->nullable();
            $table->text('remark')->nullable();
            $table->unsignedBigInteger('duration')->default(0);
            $table->decimal('overtime', 4, 2)->default(0);
            $table->decimal('hourly_rate', 4, 2)->default(0);
            $table->decimal('overtime_rate', 4, 2)->default(0);
            $table->enum('status', ['PENDING', 'APPROVED', 'CANCELLED', 'LEAVE', 'HOLIDAY', 'OFFDAY'])->default('PENDING');

            $table->timestamps();
            $table->softDeletes();


            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('time_sheets');
    }
}
