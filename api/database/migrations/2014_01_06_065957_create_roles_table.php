<?php

use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('role', 50);
            $table->text('description');
            $table->unsignedTinyInteger('is_active')->default(0);
            $table->timestamps();
        });
        // current date/time
        $now = Carbon::now()->toDateTimeString();
        // insert roles
        $roles = [
            ['role' => 'SUPER_ADMIN', 'description' => 'The provider organization admin.', 'created_at' => $now, 'updated_at' => $now],
            ['role' => 'USER_ADMIN', 'description' => 'The consumer organization admin.', 'created_at' => $now, 'updated_at' => $now],
            ['role' => 'USER_STAFF', 'description' => 'The consumer organization staff.', 'created_at' => $now, 'updated_at' => $now],
        ];
        $role = new Role();
        $role->insert($roles);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
