<?php

namespace App\Libraries;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

final class Space
{
    private $avatars;
    private $logos;
    private $photos;
    private $fileDisk = 'files';
    private $contactDisk = 'contacts';

    public function __construct()
    {
        // init the directories
        $this->avatars = 'avatars';
        $this->logos = 'logos';
        $this->photos = 'photos';
    }

    /**
     * --------------------------------------------------
     * get public URL of the uploaded file.
     * --------------------------------------------------
     * @param $bucketPath
     * @param $filename
     * @return mixed
     * --------------------------------------------------
     */
    public function getFileUrl($bucketPath, $filename)
    {
        return Storage::disk($this->fileDisk)
            ->url($bucketPath . '/' . $filename);
    }

    /**
     * --------------------------------------------------
     * method to upload the user avatars.
     * --------------------------------------------------
     * @param string $filename
     * @param mixed $content
     * @return mixed|string
     * --------------------------------------------------
     */
    public function avatar($filename, $content)
    {
        return $this->upload($this->avatars, $filename, $content);
    }

    /**
     * --------------------------------------------------
     * upload files to remote digital/amazon storage.
     * --------------------------------------------------
     * @param string $bucketPath
     * @param string $filename
     * @param string $content
     * @return string|mixed
     * --------------------------------------------------
     */
    public function upload($bucketPath, $filename, $content)
    {
        $remoteFile = $this->getFilename($filename);
        try {
            if ($bucketPath === $this->logos || $bucketPath === $this->avatars || $bucketPath === $this->photos) {
                Storage::disk($this->fileDisk)->put($bucketPath . '/' . $remoteFile, $content, 'public');
                $uploadedFile = $remoteFile;
            } else {
                $uploadedFile = Storage::disk($this->fileDisk)->putFile($bucketPath, $content, 'public');
            }
            return (object)[
                'filename' => $uploadedFile,
                'type' => $bucketPath,
                'url' => Storage::disk($this->fileDisk)->url($bucketPath . '/' . $uploadedFile)
            ];
        } catch (Exception $e) {
            return ['status' => false, 'message' => 'Failed to upload the file.', 'e' => $e->getMessage()];
        }
    }

    /**
     * --------------------------------------------------
     * concat timestamp and unique string to generate the
     * remote space file.
     * --------------------------------------------------
     * @param $filename
     * @return string
     * --------------------------------------------------
     */
    private function getFilename($filename): string
    {
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $filename = Carbon::now()->timestamp . '-' . preg_replace('/\.' . $extension . '$/im', '', $filename);
        return strtolower(preg_replace('/\W+|_/m', '-', $filename) . '.' . $extension);
    }

    /**
     * --------------------------------------------------
     * method to upload the contact photos.
     * --------------------------------------------------
     * @param string $filename
     * @param mixed $content
     * @return mixed|string
     * --------------------------------------------------
     */
    public function photo($filename, $content)
    {
        return $this->upload($this->photos, $filename, $content);
    }

    /**
     * --------------------------------------------------
     * method to upload logo for organizations.
     * --------------------------------------------------
     * @param string $filename
     * @param mixed $content
     * @return mixed|string
     * --------------------------------------------------
     */
    public function logo($filename, $content)
    {
        return $this->upload($this->logos, $filename, $content);
    }

    /**
     * --------------------------------------------------
     * resize the image to given size or default.
     * --------------------------------------------------
     * @param $original
     * @param int $size
     * @return mixed
     * --------------------------------------------------
     */
    public function resizeImage($original, int $size)
    {
        if ($size === null) {
            $size = 250;
        }
        $data = getimagesize($original);
        $originalHeight = $data['0'];
        $originalWidth = $data['1'];

        if ($originalWidth > $originalHeight) {
            $shorterLength = $originalHeight;
        } else {
            $shorterLength = $originalWidth;
        }

        $newImage = Image::make($original)
            ->crop($shorterLength, $shorterLength, 0, 0)
            ->resize($size, $size)
            ->stream();
        return $newImage;
    }
}
