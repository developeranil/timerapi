<?php

namespace App\Libraries;

class Identity
{
    /**
     * --------------------------------------------------
     * check identity for all super users.
     * --------------------------------------------------
     * @return bool
     * --------------------------------------------------
     */
    public function isSuper(): bool
    {
        return session()->get('role') === 'SUPER_ADMIN';
    }

    /**
     * --------------------------------------------------
     * check identity for super admins.
     * --------------------------------------------------
     * @return bool
     * --------------------------------------------------
     */
    public function isSuperAdmin(): bool
    {
        return session()->get('role') === 'SUPER_ADMIN';
    }

    /**
     * --------------------------------------------------
     * check identity for all user admins.
     * --------------------------------------------------
     * @return bool
     * --------------------------------------------------
     */
    public function isUser(): bool
    {
        return session()->get('role') === 'USER_ADMIN';
    }

    /**
     * --------------------------------------------------
     * check identity for user admins.
     * --------------------------------------------------
     * @return bool
     * --------------------------------------------------
     */
    public function isUserAdmin(): bool
    {
        return session()->get('role') === 'USER_ADMIN';
    }

    /**
     * --------------------------------------------------
     * check identity for user staffs.
     * --------------------------------------------------
     * @return bool
     * --------------------------------------------------
     */
    public function isUserStaff(): bool
    {
        return session()->get('role') === 'USER_STAFF';
    }
}
