<?php

namespace App\Libraries;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

final class JWTAuth extends JWTHelper
{
    private $token = null;
    private $request;

    public function __construct(Request $request)
    {
        parent::__construct($request);
        // init request and token
        $this->request = $request;
        $this->token = $request->bearerToken();
    }

    /**
     * --------------------------------------------------
     * create new JWT token using the custom data/payload.
     * --------------------------------------------------
     * @param object $user
     * @param array $extraInfo
     * @return string
     * --------------------------------------------------
     */
    public function setToken($user, $extraInfo = []): string
    {
        $fingerprint = $this->getFingerprint();
        $payload = array_merge([
            'user_id' => $user->id,
            'expire' => self::getLifetime(),
            'fp' => $fingerprint
        ], $extraInfo);
        natsort($payload);
        // update fingerprint
        $user->fingerprint = $fingerprint;
        $user->save();
        // generate token
        return $this->setPayload($payload)->getToken();
    }

    /**
     * --------------------------------------------------
     * The timestamp for end of the current english month.
     * --------------------------------------------------
     * @return int
     * --------------------------------------------------
     */
    public static function getLifetime(): int
    {
        return Carbon::now()->endOfMonth()->timestamp;
    }

    /**
     * --------------------------------------------------
     * validate the auth token and set the details in session.
     * --------------------------------------------------
     * @return bool
     * --------------------------------------------------
     */
    public function auth(): bool
    {
        // check if token exists
        if ($this->validate($this->token)) {
            $payload = $this->getPayload($this->token);
            // valid token BUT logged out
            $user = User::find($payload->user_id);
            if (isset($user->id) && $user->fingerprint === $this->getFingerprint()) {
                // update online time
                $user->last_online = Carbon::now()->timestamp;
                $user->save();
                // set session
                $this->setMulti($payload);
                return true;
            }
        }
        return false;
    }

    /**
     * --------------------------------------------------
     * set multiple session key/pair using array or object.
     * --------------------------------------------------
     * @param $params
     * @return bool
     * --------------------------------------------------
     */
    private function setMulti($params): bool
    {
        $this->request->session()->flush();
        if (!empty($params)) {
            foreach ($params as $key => $val) {
                $this->request->session()->put($key, $val);
            }
            return true;
        }
        return false;
    }
}
