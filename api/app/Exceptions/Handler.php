<?php

namespace App\Exceptions;

use App\Http\Resources\Common\MessageResource;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\UnauthorizedException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use UnexpectedValueException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Render an exception into an HTTP response.
     * @param Request $request
     * @param Exception $exception
     * @return Response
     */
    public function render($request, Exception $exception)
    {
        // unauthorized
        if ($exception instanceof UnauthorizedException) {
            return response(new MessageResource(['message' => $exception->getMessage() ?? '']), 401);
        }

        // forbidden resource
        if ($exception instanceof AuthorizationException) {
            return response(new MessageResource(['message' => $exception->getMessage() ?? 'Forbidden! You don\'t have permission to  access this resource.']), 403);
        }

        // resource not found
        if ($exception instanceof NotFoundHttpException || $exception instanceof ModelNotFoundException) {
            return response(new MessageResource(['message' => 'The resource you requested could not be found.']), 404);
        }

        // unsupported http method
        if ($exception instanceof MethodNotAllowedHttpException) {
            return response(new MessageResource(['message' => $exception->getMessage()]), 405);
        }

        // unprocessed
        if ($exception instanceof UnprocessableEntityHttpException || $exception instanceof UnexpectedValueException) {
            $message = !empty($exception->getMessage()) ? $exception->getMessage() : 'Cannot process the request.';
            return response(new MessageResource(['message' => $message]), 422);
        }

        // everything else
        return parent::render($request, $exception);
    }
}
