<?php

namespace App\Http\Resources\Common;

use App\Http\Resources\BaseResource;
use Illuminate\Http\Request;

class MessageResource extends BaseResource
{
    /**
     * Transform the resource into an array.
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'message' => isset($this->message) ? $this->message : $this['message']
        ];
    }
}
