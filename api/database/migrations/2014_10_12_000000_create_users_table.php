<?php

use App\Libraries\Common;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('role_id'); // reference to role
            $table->enum('provider', ['SYSTEM', 'GOOGLE'])->default('SYSTEM'); // auth provider
            $table->string('organization_name', 100)->nullable();
            $table->string('first_name', 50)->nullable();
            $table->string('middle_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('position', 50)->nullable();
            $table->enum('marital_status', ['MARRIED', 'UNMARRIED', 'DIVORCED'])->nullable();
            $table->enum('gender', ['M', 'F', 'N/A'])->nullable();
            $table->date('dob')->nullable();
            $table->date('joining_date')->nullable();
            $table->string('specialization')->nullable();
            $table->string('experience')->nullable();
            $table->string('educational')->nullable();
            $table->string('token', 64);
            $table->string('password', 64);
            $table->string('fingerprint', 64)->nullable();
            $table->string('photo_url')->nullable();
            $table->text('description')->nullable();
            $table->text('remark')->nullable();
            $table->unsignedBigInteger('last_online')->nullable();
            $table->unsignedTinyInteger('is_active')->default(0);
            $table->unsignedTinyInteger('is_verified')->default(0);

            $table->timestamps();
            $table->softDeletes();


            $table->foreign('role_id')
                ->references('id')
                ->on('roles')
                ->onDelete('cascade');
        });
        // seed super admin
        $user = new User();
        $user->role_id = 1;
        $user->organization_name = 'Cixware';
        $user->provider = 'SYSTEM';
        $user->first_name = 'Super';
        $user->last_name = 'Admin';
        $user->token = Common::identifier(User::class, 'token', 16);
        $user->password = password_hash('secret', PASSWORD_BCRYPT);

        $user->is_active = 1;
        $user->save();
    }

    /**
     * Reverse the migrations.
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
